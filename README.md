# docker-compose

Some useful docker-compose files. The most interesting one is the [traefik][traefik] docker-compose file.

## Traefik with Let's encrypt
With the Traefik router, all access to services will be routed through it. All other services should not be accessible from outside. The configuration for the different services behind the Traefik router are configured in the docker-compose files of the services itself.

The following environment variables can be configured in an `.env` file:

| Variable             | Description                   |
|----------------------|-------------------------------|
| ACME_EMAIL            | E-Mail address for Let'encrypt|
| TRAEFIK_DOCKER_DOMAIN | Domain name                   |


## Nextcloud
With the [docker-compose file][nextcloud], a Nextcloud instance can be started behind a Traefik router. 
The following environment variables can be configured in an `.env` file:

| Variable              | Description                   |
|-----------------------|-------------------------------|
| HOST                  | Domain name                   |


<!-- identifiers -->
[traefik]: traefik/docker-compose.yml
[nextcloud]: nextcloud/docker-compose.yml
